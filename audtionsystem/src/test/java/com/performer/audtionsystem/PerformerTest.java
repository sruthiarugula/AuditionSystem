package com.performer.audtionsystem;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PerformerTest {
	Audition audition;
	OtherPerformer performer;

	@Before

	public void doSetUp() {

	}

	@Test

	public void PerformerPerformanceTest() {
		// TODO Auto-generated method stub

		Performers performer1 = new OtherPerformer(124);

		String expected = "124 - performer";
		String actual = performer1.givePerformance();
		assertEquals(expected, actual);

	}
	@Test
	public void PerformerPerformanceNegativeTest() {
		// TODO Auto-generated method stub

		Performers performer1 = new OtherPerformer(125);

		String expected = "125-performer";
		String actual = performer1.givePerformance();
		assertEquals(expected, actual);

	}

}
