package com.performer.audtionsystem;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

public class AuditionTest {
	Audition audition;

	@Before

	public void doSetUp() {
		audition = new Audition();
	}

	@Test
	public void Auditiontest() {

		String expected = "128 - performer" + '\n' + "129 - performer" + '\n' + "123 - performer" + '\n'
				+ "124 - performer" + '\n' + "tap - 125 - dancer" + '\n' + "jumba - 126 - dancer" + '\n'
				+ "I sing in the key of - G - at the volume 10 - 127" + '\n';
		String actual = audition.audtionDay();
		assertEquals(expected, actual);

	}

	@Test
	public void uniqueIdGenerator() {
		// TODO Auto-generated method stub
		Set<Integer> actual = audition.uniqueIdGenerator();
		Set<Integer> expected = new HashSet<Integer>();
		expected.add(123);
		expected.add(123);
		expected.add(124);
		expected.add(125);
		expected.add(126);
		expected.add(127);
		expected.add(128);
		expected.add(129);
		assertEquals(expected, actual);
	}

}
