package com.performer.audtionsystem;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.performance.logging.PerformanceLogging;

public class Audition {
	Performers performer1;
	Performers performer2;
	Performers performer3;
	Performers performer4;
	Performers dancer1;
	Performers dancer2;
	Performers vocalist1;

	

	public String audtionDay() {
		Set<Integer> set = uniqueIdGenerator();
		List<Integer> list = new ArrayList<Integer>(set);

		// 4 performers, 2 dancers and a vocalist for a total of 7 performers.
		// Ensure that this is testable.

		performer1 = new OtherPerformer(list.get(0));
		performer2 = new OtherPerformer(list.get(1));
		performer3 = new OtherPerformer(list.get(2));
		performer4 = new OtherPerformer(list.get(3));
		dancer1 = new Dancer(list.get(4), "tap");
		dancer2 = new Dancer(list.get(5), "jumba");
		vocalist1 = new Vocalist("G", list.get(6), 10);
		String value = performer1.givePerformance() + '\n' + performer2.givePerformance() + '\n'
				+ performer3.givePerformance() + '\n' + performer4.givePerformance() + '\n' + dancer1.givePerformance()
				+ '\n' + dancer2.givePerformance() + '\n' + vocalist1.givePerformance() + '\n';
		//PerformanceLogging logging = new PerformanceLogging();
		//logging.logPerformance();

		return value;
	}

	// To generate UniqueIds
	Set<Integer> uniqueIdGenerator() {

		Set<Integer> set = new HashSet<Integer>();
		set.add(123);
		set.add(123);
		set.add(124);
		set.add(125);
		set.add(126);
		set.add(127);
		set.add(128);
		set.add(129);

		return set;
	}

}
